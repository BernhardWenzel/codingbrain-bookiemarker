$(document).ready(function () {

    $(".discard").click(function(e){
        e.preventDefault();
        var url = $(this).data("url");
        bootbox.confirm({
            message: "Do you really want to discard your changes? This can't be undone.",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result){
                    $.get(url, function (data) {
                        showMessage(data.content)
                    })
                }
            }
        });
    });

    $(".store").click(function(e){
        e.preventDefault();
        var url = $(this).data("url");
        bootbox.confirm({
            message: "Do you really want to commit your changes, overriding previous commits?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result){
                    $.get(url, function (data) {
                        showMessage(data.content)
                    })
                }
            }
        });
    });


    $(".checkout").click(function(e){
        e.preventDefault();
        var url = $(this).data("url");
        var repo = $(this).data("repo");
        $.get("/is_dirty/" + repo, function (data) {
            if (data.content.is_dirty){
                bootbox.prompt({
                    title: "You have local changes. Before switching the a new branch, what do you want to do with your changes?",
                    inputType: 'select',
                    inputOptions: [
                        {
                            text: 'Choose one...',
                            value: '',
                        },
                        {
                            text: 'Discard your changes. Warning: this can\'t be undone!',
                            value: '3',
                        },
                        {
                            text: 'Commit into "mysolution", overriding previous changes.',
                            value: '1',
                        }/*,
                        {
                            text: 'Merge into "mysolution", allowing conflicts. This may require to fix them manually.',
                            value: '2',
                        }*/,
                        {
                            text: 'Cancel, merge manually using git.',
                            value: '4',
                        }
                    ],
                    callback: function (result) {
                        var value = result;
                        var git_url = "cancel";
                        if (value == 1){
                            git_url = "/git_merge/" + repo;
                        } else if (value == 2){
                            git_url = "/git_merge/" + repo + "?allow_conflicts=True";
                        } else if (value == 3){
                            git_url = "/git_reset/" + repo;
                        }
                        if (git_url != "cancel") {
                            $.get(git_url, function (data) {
                                showMessage(data.content);
                            }).fail(function(data){
                                showMessage(data.content);
                            }).done(function () {
                                $.get(url, function (data) {
                                    showMessage(data.content);
                                });
                            });
                        }
                    }
                });
            } else {
                $.get(url, function (data) {
                    showMessage(data.content);
                });
            }
        })
    });

});

function showMessage(content) {
    $.notify({
        // options
        message: content.message
    },{
        // settings
        type: content.status,
        placement: {
            align: "center"
        }
    });
}