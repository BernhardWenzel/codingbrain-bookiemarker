$(document).ready(function () {
    ping();
});

function ping(){
    var vagrantMsg = $("#vagrantMessage");
    $.get('/ping/', function(data) {
        ['frontend', 'backend', 'worker'].forEach(function (service) {
            var d = data.content;

            // status
            var status = d[service].status;
            var statusSelector = "#" + service + "Status";
            if ($(statusSelector).length){
                var serviceStatus= $(statusSelector).removeClass();
                serviceStatus.removeClass();
                serviceStatus.addClass("status"+status);
                serviceStatus.text(status);
                if (status == 'up' || status == 'starting'){
                    $("#"+service+"Start").hide();
                    $("#"+service+"Stop").show();
                } else {
                    $("#"+service+"Start").show();
                    $("#"+service+"Stop").hide();
                }
            }

            // branches
            var currentBranch = d[service].branch;
            
            var branchSelector = $("#" + service + "Branch");
            branchSelector.text(currentBranch);
            var branchFinalSelector = $("#"+service+ "BranchFinal");
            if (currentBranch != 'final' && currentBranch != 'n/a'){
                branchFinalSelector.show();
            } else {
                branchFinalSelector.hide();
            }
            var branchMysolutionSelector = $("#"+service+ "BranchMysolution");
            if (currentBranch != 'mysolution' && currentBranch != 'n/a'){
                branchMysolutionSelector.show();
            } else {
                branchMysolutionSelector.hide();
            }

            var isDirty = d[service].is_dirty;
            var isDirtySelector = $("#" + service + "IsDirty");
            if (isDirty) {
                isDirtySelector.show();
            } else {
                isDirtySelector.hide();
            }


            var isConflicts = d[service].has_conflicts;
            var isConflictsSelector = $("#" + service + "Conflicts");
            if (isConflicts) {
                isDirtySelector.hide();
                isConflictsSelector.show();
            } else {
                isConflictsSelector.hide();
            }

            // stashes
            /*var stashes = d[service].stashes;
            var stashSelector = $("#" + service + "Stash");
            var stashPop = $("#" + service + "StashPop");
            var stashMerge = $("#" + service + "StashMerge");
            if (stashes > 0) {
                stashSelector.show();
                $("#" + service + "StashCount").text(stashes);
                if (d[service].is_dirty){
                    stashPop.hide();
                    stashMerge.show();
                } else {
                    stashPop.show();
                    stashMerge.hide();
                }
            } else {
                stashSelector.hide();
            }*/
        });
    }).fail(function(){
        if (vagrantMsg.is(":hidden")){
            vagrantMsg.modal({"show": true, "backdrop": "static", "keyboard": false});
        }
    }).done(function() {
        if (vagrantMsg.is(":visible")) {
            vagrantMsg.modal("hide");
        }
    });
    setTimeout(ping, 1000);
}