from django import template

register = template.Library()


@register.filter(is_safe=True)
def as_label(value, default=None):
    if default and "label" in default:
        return default["label"]
    return value\
        .replace("-", " ")\
        .replace("solution", "solution: ") \
        .replace("code", "let's code: ") \
        .capitalize()