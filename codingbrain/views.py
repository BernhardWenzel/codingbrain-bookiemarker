import copy
import os

import subprocess
from django.http import HttpResponseBadRequest, Http404
from django.shortcuts import render, redirect
from django_ajax.decorators import ajax
from git import Repo
from codingbrain.lessons import *


def index(request):
    session_lesson = request.session.get('current_lesson')
    current_lesson = session_lesson if session_lesson is not None else "start-here"
    return redirect('lesson', name=current_lesson)


def lesson(request, name):
    # load lesson content if available
    session_lessons = request.session.get('lessons')
    lessons = load_lessons(session_lessons)
    if not session_lessons:
        request.session['lessons'] = lessons
    toc = build_toc(lessons["units"])
    if name == "toc":
        context = {"toc" : toc}
        return render(request, 'codingbrain/lessons/toc-only.html', context)
    else:
        request.session['current_lesson'] = name
        context = lessons["units"][name]
        if name not in lessons["units"]:
            return render(request, 'codingbrain/lessons/%s.html' % name)
        # the toc of this chapter
        context["parent"], context["parent_label"], context["lesson_toc"] = get_toc_parent(name, toc["lessons"])

        # use lesson_id to generate class_link
        if "lesson_id" in context:
            context["class_url"] = settings.CLASS_URL.format(str(context["lesson_id"]))

        # the toc of the whole course
        context["toc"] = toc
        context["settings"] = lessons["settings"]
        context["current"] = name
        if ("pre" not in context or not context["pre"]) and ("next" not in context or not context["next"]):
            context["pre"], context["pre_label"], context["next"], context["next_label"] = get_next_lessons(name, lessons["units"])
        if "title" not in context or not context["title"]:
            context["title"] = filters.as_label(name)
        if "label" not in context or not context["label"]:
            context["label"] = filters.as_label(name)

        return render(request, 'codingbrain/lessons/lessons.html', context)


@ajax
def update_lessons(request):
    current_version = -1
    lessons = request.session.get('lessons')
    if lessons:
        current_version = lessons["settings"]["version"]
    lessons = load_lessons(lessons, force=True)
    request.session['lessons'] = copy.deepcopy(lessons)
    msg = "Lessons updated to latest version"
    status = "success"
    if current_version == lessons["settings"]["version"]:
        msg = "Nothing updated, you have the latest version"
        status = "info"
    return {"message": msg, "status": status}


@ajax
def run(request, service):
    p = subprocess.Popen(['sudo', 'start', service], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return {"message": "Starting " + service + ". It may take a while until it is up.",
            "status": "success"}


@ajax
def stop(request, service):
    p = subprocess.Popen(['sudo', 'stop', service], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    return {"message": "Stopping " + service.capitalize() + ".",
            "status": "success"}


@ajax
def ping(request):
    p = subprocess.check_output(['ps', 'auxww'])
    # backend
    backend_status = "down"
    if str(p).find("/vagrant/backend/build/resources/main:") != -1:
        # process is available
        backend_status = "starting"
        # backend already up?
        try:
            r = requests.get(settings.BACKEND_URL)
            if r.status_code == 200:
                backend_status = "up"
        except:
            pass
    # "worker":
    worker_status = "down"
    if str(p).find("python3 worker.py") != -1:
        worker_status = "up"

    json_response = {"backend": {"status": backend_status, "branch": "n/a"},
                     "worker": {"status": worker_status, "branch": "n/a"},
                     "frontend": {"status": "up", "branch": "n/a"}}

    # get git branches & stashes
    for repository in ['frontend', 'backend', 'worker']:
        repo_path = settings.CODE_PATH + '/' + repository
        if os.path.exists(repo_path):
            repo = Repo(repo_path)
            json_response[repository]['branch'] = repo.active_branch.name
            git = repo.git
            status = git.status("--short")
            json_response[repository]['has_conflicts'] = "UU" in status
            json_response[repository]['is_dirty'] = repo.is_dirty(untracked_files=True)

    return json_response


@ajax
def checkout(request, repository, branch):
    if repository not in settings.GIT_REPOS:
        return HttpResponseBadRequest('<h1>Request not found</h1>')

    repo_path = settings.CODE_PATH + '/' + repository
    if not os.path.exists(repo_path):
        # clone
        repo = Repo.clone_from(settings.GIT_REPOS.get(repository), repo_path)
    else:
        repo = Repo(repo_path)
    assert not repo.bare

    git = repo.git
    git.config("user.email", "support@codingbrain.com")
    git.config("user.name", "vagrant")

    # checkout branch
    if branch == settings.SOLUTION_BRANCH:
        current_branch = repo.active_branch.name
        branch_number = current_branch.replace("solution", "").replace("lesson", "")
        branch = settings.SOLUTION_BRANCH + branch_number
        # create if not exists
        branches = git.branch()
        if not branch in branches:
            git.branch(branch)
    try:
        git.checkout(branch)
    except Exception as e:
        return {"message": "Could not check out " + repository + " branch: '" + branch + "'",
                "status": "danger"}
    return {"message": "Successfully checked out " + repository + " branch: '" + branch + "'",
            "status": "success"}

@ajax
def is_dirty(request, repository):
    if repository not in settings.GIT_REPOS:
        return HttpResponseBadRequest('<h1>Request not found</h1>')

    repo_path = settings.CODE_PATH + '/' + repository
    if not os.path.exists(repo_path):
        # clone
        repo = Repo.clone_from(settings.GIT_REPOS.get(repository), repo_path)
    else:
        repo = Repo(repo_path)
    assert not repo.bare

    return {"is_dirty": repo.is_dirty(untracked_files=True)}



@ajax
def git_merge_mysolution(request, repository, allow_conflicts=False):
    if repository not in settings.GIT_REPOS:
        return HttpResponseBadRequest('<h1>Request not found</h1>')

    repo_path = settings.CODE_PATH + '/' + repository
    if not os.path.exists(repo_path):
        return HttpResponseBadRequest("Repository does not exist")
    else:
        repo = Repo(repo_path)
        assert not repo.bare

        git = repo.git
        git.config("user.email", "support@codingbrain.com")
        git.config("user.name", "vagrant")

        current_branch = repo.active_branch.name
        branch_number = current_branch.replace("solution", "").replace("lesson", "")
        new_branch = settings.SOLUTION_BRANCH + branch_number

        # create if not exists
        branches = git.branch()
        if not new_branch in branches:
            git.branch(new_branch)

        allow_conflicts = request.GET.get('allow_conflicts', False)

        try:
            git.stash('save')
            if allow_conflicts:
                git.checkout(new_branch)
            else:
                git.checkout("-B", new_branch)
            git.stash('pop')
            git.commit('-a', '-m', "merged " + current_branch)
        except Exception as e:
            return {"message": "Could not commit your changes into the '" + new_branch + "' branch",
            "status": "danger"}

        return {"message": "Your changes have been committed into the '" + new_branch + "' branch",
            "status": "success"}


@ajax
def git_reset(request, repository):
    if repository not in settings.GIT_REPOS:
        return HttpResponseBadRequest('<h1>Request not found</h1>')

    repo_path = settings.CODE_PATH + '/' + repository
    if not os.path.exists(repo_path):
        return HttpResponseBadRequest("Repository does not exist")
    else:
        repo = Repo(repo_path)
        assert not repo.bare
        repo.git.reset('--hard')
        repo.git.clean('-f', '-d')

        return {"message": "Successfully reset your changes",
                "status": "success"}


@ajax
def pop_stash(request, repository):
    if repository not in settings.GIT_REPOS:
        return HttpResponseBadRequest('<h1>Request not found</h1>')

    repo_path = settings.CODE_PATH + '/' + repository
    if not os.path.exists(repo_path):
        return HttpResponseBadRequest("Repository does not exist")
    else:
        repo = Repo(repo_path)
        assert not repo.bare
        repo.git.stash("pop")

        return {"message": "Successfully popped stash",
                "status": "success"}
