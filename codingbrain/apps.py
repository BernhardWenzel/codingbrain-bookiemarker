from django.apps import AppConfig


class CodingbrainConfig(AppConfig):
    name = 'codingbrain'