from django.conf import settings


def ping_enabled(request):
    return {'ping_enabled': settings.PING_ENABLED }


def version(request):
    return {'cb_version': settings.VERSION }

