from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from codingbrain import views

urlpatterns = [
    url(r'^checkout/(?P<repository>[\w-]+)/(?P<branch>[\w-]+)/$', views.checkout, name='checkout'),
    url(r'^run/(?P<service>[\w-]+)/$', views.run, name='run'),
    url(r'^stop/(?P<service>[\w-]+)/$', views.stop, name='stop'),
    url(r'^is_dirty/(?P<repository>[\w-]+)/$', views.is_dirty, name='is_dirty'),
    url(r'^git_merge/(?P<repository>[\w-]+)/$', views.git_merge_mysolution, name='git_merge'),
    url(r'^git_reset/(?P<repository>[\w-]+)/$', views.git_reset, name='git_reset'),
    url(r'^update_lessons/$', views.update_lessons, name='update_lessons'),
    url(r'^ping/$', views.ping, name='ping'),
    url(r'^lesson/(?P<name>[\w-]+)/$', views.lesson, name='lesson'),
    url(r'^start$', views.index, name='index'),
    url(r'^$', views.index, name='index'),
]

urlpatterns += staticfiles_urlpatterns()