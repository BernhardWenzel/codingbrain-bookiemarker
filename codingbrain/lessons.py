from django.conf import settings
import requests, yaml, codecs
from collections import OrderedDict

from codingbrain.templatetags import filters


def get_next_lessons(current, lessons):
    next = None
    next_label = None
    pre = None
    pre_label = None
    lessons_list = list(lessons.keys())
    for index, lesson in enumerate(lessons_list):
        if lesson == current:
            if index + 1 < len(lessons):
                next = lessons_list[index + 1]
                next_label = lessons.get(next).get("label", filters.as_label(next))
            if index > 0:
                pre = lessons_list[index - 1]
                pre_label = lessons.get(pre).get("label", filters.as_label(pre))
            break
    return pre, pre_label, next, next_label


def build_toc(lessons):
    lessons_0 = []
    lessons_1 = []
    lessons_2 = []
    lessons_toc = OrderedDict()
    lessons_toc["lessons"] = lessons_0
    parent_0 = None
    parent_0_label = None
    parent_1 = None
    parent_1_label = None
    for name, lesson in lessons.items():
        if "level" in lesson:
            level = lesson.get("level")
            label = lesson.get("label")
            if not label:
                label = filters.as_label(name)
            current_lesson = OrderedDict()
            current_lesson["lesson"] = name
            current_lesson["label"] = label
            # same level = add to lessons array
            if level == 0:
                parent_0 = name
                parent_0_label = label
                lessons_1 = []
                current_lesson["lessons"] = lessons_1
                lessons_0.append(current_lesson)
            # higher level = add new lessons array
            elif level == 1:
                parent_1 = name
                parent_1_label = label
                current_lesson["parent"] = parent_0
                current_lesson["parent_label"] = parent_0_label
                lessons_1.append(current_lesson)
                lessons_2 = []
                current_lesson["lessons"] = lessons_2
            elif level == 2:
                current_lesson["parent"] = parent_1
                current_lesson["parent_label"] = parent_1_label
                lessons_2.append(current_lesson)
    return lessons_toc


def get_toc_parent(name, toc_lessons):
    parent = None
    parent_label = None
    lessons_toc = None
    for l in toc_lessons:
        if l["lesson"] == name:
            if "parent" in l:
                parent = l["parent"]
                parent_label = l["parent_label"]
            if "lessons" in l and l["lessons"]:
                lessons_toc = l
            return parent, parent_label, lessons_toc
        for ll in l["lessons"]:
            if ll["lesson"] == name:
                if "parent" in ll:
                    parent = ll["parent"]
                    parent_label = ll["parent_label"]
                if "lessons" in ll and ll["lessons"]:
                    lessons_toc = ll
                return parent, parent_label, lessons_toc
            for lll in ll["lessons"]:
                if lll["lesson"] == name:
                    if "parent" in lll:
                        parent = lll["parent"]
                        parent_label = lll["parent_label"]
                    if "lessons" in lll and lll["lessons"]:
                        lessons_toc = lll
                    return parent, parent_label, lessons_toc
    return parent, parent_label, lessons_toc


def load_lessons(session_lesson, force=False):
    if not session_lesson or force or settings.LOCAL_YAML:
        if settings.LOCAL_YAML:
            file = codecs.open(settings.LOCAL_YAML, "r", "utf-8").read()
            lessons = ordered_load(file)
        else:
            file = requests.get(settings.LESSONS_YAML_URL)
            lessons = ordered_load(file.content)
        _settings = lessons.pop("settings")
        _lessons = {"units": OrderedDict(lessons), "settings": _settings}
        return _lessons
    else:
        return session_lesson


def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)